/**
 * 
 */
package sdsl.chainreaction;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import sdsl.chainreaction.blocks.BlockCreator;
import sdsl.chainreaction.controller.KycDbController;
import sdsl.chainreaction.exceptions.ConfigException;
import sdsl.chainreaction.exceptions.DBException;
import sdsl.chainreaction.transactions.Transaction;
import sdsl.chainreaction.utils.KycConfiguartor;
import sdsl.chainreaction.utils.SignatureUtil;

/**
 * @author Sudip Das
 *
 */
public class KycDemo {
	/**
	 * Default logger
	 */
	static Logger logger = null;
	/**
	 * Database controller
	 */
	private KycDbController controller = null;
	// The master Key
	private SignatureUtil sumyagKeys = null;

	// The user keys
	LinkedHashMap<String, SignatureUtil> ownerAndSigningKeysMap = new LinkedHashMap<String, SignatureUtil>();

	LinkedHashMap<String, Transaction> ownerAndOwnerBlockTxnMaps = new LinkedHashMap<String, Transaction>();

	public KycDemo(String currentEnv) throws ConfigException, DBException {
		KycConfiguartor.init(currentEnv);
		logger = Logger.getLogger(KycDemo.class.getName());
		controller = new KycDbController();
		// Setting the master keys
		sumyagKeys = new SignatureUtil();
	}

	public SignatureUtil getSumyagKeys() {
		return sumyagKeys;
	}

	public ArrayList<ArrayList<Transaction>> createSignedBlocks() throws DBException {
		ArrayList<LinkedHashMap<String, Object>> kycHashRows = new ArrayList<LinkedHashMap<String, Object>>();
		ArrayList<LinkedHashMap<String, Object>> kycRetrievalRows = new ArrayList<LinkedHashMap<String, Object>>();
		ArrayList<ArrayList<Transaction>> finalBlockChain = new ArrayList<ArrayList<Transaction>>();

		ArrayList<String> jobOwners = controller.getOwners();
		LinkedHashMap<String, Transaction> ownerTxnMaps = new LinkedHashMap<String, Transaction>();
//		System.err.println("all owners:" + jobOwners);
		for (String eachOwner : jobOwners) {
			logger.info("Creating Blocks for owner:" + eachOwner);
			/*
			 * Generate Public, Private Keys for Owner and populate ownerAndSigningKeysMap
			 * of owner with security keys; Generate Hash for Owner, Transaction for owner
			 * with Hash of owner
			 */
			logger.debug("Generating Security Keys for Owner:" + eachOwner);
			ownerAndSigningKeysMap.put(eachOwner, new SignatureUtil());
			logger.debug("Generating Hash for Owner:" + eachOwner);
			BlockCreator ownerBlock = new BlockCreator(eachOwner, "0");
			logger.debug("Generating Transaction for Owner:" + eachOwner);
			Transaction ownerTxn = getSignedTrasactionData(eachOwner, ownerBlock.hash);
			ownerTxnMaps.put(eachOwner, ownerTxn);
			ownerAndOwnerBlockTxnMaps.put(eachOwner, ownerTxn);

			// Associating each owner with jobids
			ArrayList<String> associatedJobIds = controller.getJobIds(eachOwner);
			LinkedHashMap<String, Transaction> jobIdTxnMaps = new LinkedHashMap<String, Transaction>();
//			System.out.println(eachOwner + ":" + associatedJobIds);
			for (String eachJobId : associatedJobIds) {
				logger.debug("Generating Hash for JobId:" + eachJobId);
				BlockCreator jobIdBlock = new BlockCreator(eachJobId, ownerBlock.hash);
				logger.debug("Generating Transaction for JobId:" + eachJobId);
				Transaction jobIdTxn = getSignedTrasactionData(eachOwner, jobIdBlock.hash);
				jobIdTxnMaps.put(eachJobId, jobIdTxn);

				// Associating each jobid with fileIds
				ArrayList<String> associatedFileIds = controller.getFileIds(eachJobId);
				LinkedHashMap<String, Transaction> fileIdTxnMaps = new LinkedHashMap<String, Transaction>();

				for (String eachFileId : associatedFileIds) {// Associating each fileId with extracts
					logger.debug("Generating Hash for FileId:" + eachFileId);
					BlockCreator fileIdBlock = new BlockCreator(eachFileId, jobIdBlock.hash);
					logger.debug("Generating Transaction for FileId:" + eachFileId);
					Transaction fileIdTxn = getSignedTrasactionData(eachOwner, fileIdBlock.hash);
					fileIdTxnMaps.put(eachFileId, fileIdTxn);

					// Associating each fileId with extracts
					ArrayList<String> associatedExtractIds = controller.getExtractIds(eachFileId);
					LinkedHashMap<String, Transaction> extractsIdTxnMaps = new LinkedHashMap<String, Transaction>();

					for (String eachExtractId : associatedExtractIds) {
						logger.debug("Generating Hash for ExtractId:" + eachExtractId);
						BlockCreator extractIdBlock = new BlockCreator(eachExtractId, fileIdBlock.hash);
						logger.debug("Generating Transaction for ExtractId:" + eachExtractId);
						Transaction extractIdTxn = getSignedTrasactionData(eachOwner, extractIdBlock.hash);
						extractsIdTxnMaps.put(eachExtractId, extractIdTxn);

						// Associating each extractJson with extractId
						ArrayList<LinkedHashMap<String, String>> associatedExtractsList = controller
								.getExtracts(eachExtractId);
						for (LinkedHashMap<String, String> eachExtractMap : associatedExtractsList) {
							LinkedHashMap<String, Object> currentKycHashRecord = new LinkedHashMap<String, Object>();
							LinkedHashMap<String, Object> currentKycRetrievalRecord = new LinkedHashMap<String, Object>();
							ArrayList<Transaction> currentRecordTxns = new ArrayList<Transaction>();
							currentRecordTxns.add(ownerTxn);
							currentRecordTxns.add(jobIdTxn);
							currentRecordTxns.add(fileIdTxn);
							currentRecordTxns.add(extractIdTxn);

							// extract_id is not taken
							currentKycHashRecord.put("owner_hash", ownerTxn);
							currentKycHashRecord.put("jobid_hash", jobIdTxn);
							currentKycHashRecord.put("fileid_hash", fileIdTxn);

							currentKycRetrievalRecord.put("fileid_hash", fileIdTxn);

							// For sys_json
							logger.debug("Generating Hash for Extract:" + eachExtractMap);
							BlockCreator extractsBlock = new BlockCreator(eachExtractMap.get("sys_json"),
									extractIdBlock.hash);
							logger.debug("Generating Transaction for Extract-Json:" + eachExtractMap.get("sys_json"));
							Transaction extractsTxn = getSignedTrasactionData(eachOwner, extractsBlock.hash);
							logger.debug("Transaction for Extract-Json:" + extractsTxn);
							currentRecordTxns.add(extractsTxn);
							finalBlockChain.add(currentRecordTxns);
							currentKycHashRecord.put("sysjson_hash", extractsTxn);
							currentKycRetrievalRecord.put("sysjson", eachExtractMap.get("sys_json"));

							// For file_path
							logger.debug("Generating Hash for Extract:" + eachExtractMap);
							extractsBlock = new BlockCreator(eachExtractMap.get("file_path"), extractIdBlock.hash);
							logger.debug(
									"Generating Transaction for Extract-File Path:" + eachExtractMap.get("filepath"));
							extractsTxn = getSignedTrasactionData(eachOwner, extractsBlock.hash);
							logger.debug("Transaction for Extract-File Path:" + extractsTxn);
							currentRecordTxns.add(extractsTxn);
							finalBlockChain.add(currentRecordTxns);
							currentKycHashRecord.put("filepath_hash", extractsTxn);
							currentKycRetrievalRecord.put("filepath", eachExtractMap.get("file_path"));

							// For file_name
							logger.debug("Generating Hash for Extract:" + eachExtractMap);
							extractsBlock = new BlockCreator(eachExtractMap.get("file_name"), extractIdBlock.hash);
							logger.debug(
									"Generating Transaction for Extract-File Name:" + eachExtractMap.get("file_name"));
							extractsTxn = getSignedTrasactionData(eachOwner, extractsBlock.hash);
							logger.debug("Transaction for Extract-File Name:" + extractsTxn);
							currentRecordTxns.add(extractsTxn);
							finalBlockChain.add(currentRecordTxns);
							currentKycHashRecord.put("filename_hash", extractsTxn);
							currentKycRetrievalRecord.put("filename", eachExtractMap.get("file_name"));
							kycHashRows.add(currentKycHashRecord);
							kycRetrievalRows.add(currentKycRetrievalRecord);
//							System.err.println(finalBlockChain);
						}
					}
				}

			}
		}
//		System.err.println(dbRows.size());
//		Gson gson=new Gson();
//		String jsonStr=gson.toJson(finalBlockChain);
//		System.err.println(jsonStr);

		// Updating HashRecords in kychash
		controller.insertHashRecords(kycHashRows);
		// Updating HashRecords in kycretrieval
		controller.insertRetrievalRecords(kycRetrievalRows);

		logger.info("Completed Blockchain");
		return finalBlockChain;
	}

	public Transaction getSignedTrasactionData(String jobOwner, String data) {
		PublicKey sumyagPuBkey = getSumyagKeys().publicKey;
		PublicKey userPubKey = ownerAndSigningKeysMap.get(jobOwner).publicKey;
		Transaction transaction = new Transaction(sumyagPuBkey, userPubKey, data, null);
		transaction.generateSignature(getSumyagKeys().privateKey);
		return transaction;
	}

	public boolean validateUserAccess(String ownerId, PublicKey ownKey) {
		Transaction currTxn = ownerAndOwnerBlockTxnMaps.get(ownerId);
		if (currTxn == null) {
			logger.info("Transactions for the user:" + ownerId + " does not exist");
			return false;
		} else if (!currTxn.getReciepient().equals(ownKey)) {
			logger.info("For user:" + ownerId + ", Public Key does not match.");
			return false;
		} else {
			if (currTxn.verifySignature()) {
				logger.info("Valid user :" + ownerId + ", with correct Public Key.");
			}
			return true;
		}
	}

	public ArrayList<String> getAllFileNames(String ownerHash) throws DBException {
		ArrayList<String> fileNames = new ArrayList<String>();
		ArrayList<String> fileIdsHash = controller.getFileIdHashForOwnerHash(ownerHash);
		for (String eachFileIdHash : fileIdsHash) {
			fileNames.addAll(controller.getFileNameFromFileIdHash(eachFileIdHash));
		}
		return fileNames;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Environment not metioned. Aborting...");
			System.exit(1);
		}
		String currentEnv = args[0].trim().toLowerCase();

		System.out.println("*******************************************************************************");
		System.out.println("		KYC BlockChain Sumyag Version 1.0.0");
		System.out.println("*******************************************************************************");
		System.out.println();
		System.out.println("            #########################################");
		System.out.println("			Build Tag 20200707");
		System.out.println("            #########################################");
		System.out.println();

		try {
			KycDemo demo = new KycDemo(currentEnv);
			demo.createSignedBlocks();

			logger.info("...............Case: Access violation - Wrong public key...............");
			// Change according to what is available in extracts table
			String actualOwnerId = "akash@sumyag.com";
			String faultyOwnerId = "gowtham@sumyag.com";
			PublicKey wrongPubKey = demo.ownerAndSigningKeysMap.get(faultyOwnerId).publicKey;// Pk of gowtham
			demo.validateUserAccess(actualOwnerId, wrongPubKey);

			logger.info("...............Case: Correct public key, same user...............");
			PublicKey correctPubKey = demo.ownerAndSigningKeysMap.get(actualOwnerId).publicKey;
			boolean validUser = demo.validateUserAccess(actualOwnerId, correctPubKey);
			if (validUser) {
				Transaction currTxn = demo.ownerAndOwnerBlockTxnMaps.get(actualOwnerId);
				ArrayList<String> fileNames = demo.getAllFileNames(currTxn.toString());
				System.err.println("Files of user " + actualOwnerId + ":" + fileNames);
			}

		} catch (ConfigException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}

	}

}
