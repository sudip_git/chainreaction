/**
 * 
 */
package sdsl.chainreaction.trace;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import sdsl.chainreaction.blocks.BlockCreator;
import sdsl.chainreaction.controller.DbController;
import sdsl.chainreaction.exceptions.ConfigException;
import sdsl.chainreaction.exceptions.DBException;
import sdsl.chainreaction.transactions.Transaction;
import sdsl.chainreaction.utils.ChainConfiguartor;
import sdsl.chainreaction.utils.SignatureUtil;

/**
 * @author Sudip Das
 *
 */
public class TraceCreator {
	/**
	 * Default logger
	 */
	static Logger logger = null;

//	private ClassLoader classloader = Thread.currentThread().getContextClassLoader();

	/**
	 * Database controller
	 */
	private DbController controller = null;

	public static int difficulty = 1;
	/**
	 * Entire hashed block chain data stored as owner->jobIds->packIds->fileIds
	 */

	Multimap<String, Multimap<String, Multimap<String, String>>> hashedOwnerWithJobIdsMMap = HashMultimap.create();

	LinkedHashMap<String, String> ownerHashWithIdMap = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> jobHashWithIdMap = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> packHashWithIdMap = new LinkedHashMap<String, String>();
	LinkedHashMap<String, String> fileHashWithIdMap = new LinkedHashMap<String, String>();

//	LinkedHashMap<String, String> ownerHashMap = new LinkedHashMap<String, String>();
	LinkedHashMap<String, Transaction> ownerBlockAndTxnMaps = new LinkedHashMap<String, Transaction>();

	/*
	 * All User Signatures. Can be pused to DB
	 */
	// The master Key
	private SignatureUtil sumyagKeys = null;

	// The user keys
	LinkedHashMap<String, SignatureUtil> ownerAndSigningKeysMap = new LinkedHashMap<String, SignatureUtil>();

	LinkedHashMap<String, Transaction> ownerAndOwnerBlockTxnMaps = new LinkedHashMap<String, Transaction>();

	public TraceCreator(String currentEnv) throws ConfigException, DBException {
		ChainConfiguartor.init(currentEnv);
		logger = Logger.getLogger(TraceCreator.class.getName());
//		System.err.println(ChainConfiguartor.dbProps);
		controller = new DbController();
		// Setting the master keys
		sumyagKeys = new SignatureUtil();
	}

	public SignatureUtil getSumyagKeys() {
		return sumyagKeys;
	}

	public void generateUserSignatures(Set<String> jobOwners) {

		for (String eachOwner : jobOwners) {
			SignatureUtil userKeys = new SignatureUtil();
			ownerAndSigningKeysMap.put(eachOwner, userKeys);
		}
	}


	public Transaction getSignedTrasactionData(String jobOwner, String data) {
		PublicKey sumyagPuBkey = getSumyagKeys().publicKey;
		PublicKey userPubKey = ownerAndSigningKeysMap.get(jobOwner).publicKey;
		Transaction transaction = new Transaction(sumyagPuBkey, userPubKey, data, null);
		transaction.generateSignature(getSumyagKeys().privateKey);
		return transaction;
	}

	public void createSignedBlockChainFileTrace() throws DBException {

		Multimap<String, Multimap<String, Multimap<String, String>>> ownerToJobIdToPackIdsMMap = controller
				.getDataForChainFilesTrace();
		logger.debug("Data from DB:" + ownerToJobIdToPackIdsMMap);

		logger.info("Generating public and private keys for available job owners.................");
		generateUserSignatures(ownerToJobIdToPackIdsMMap.keySet());

		logger.debug("Creating blocks with hashed values.");

		for (String eachOwner : ownerToJobIdToPackIdsMMap.keySet()) {
			BlockCreator ownerBlock = new BlockCreator(eachOwner, "0");
			ownerHashWithIdMap.put(ownerBlock.hash, eachOwner);

			logger.info("Creating Transaction for owner:" + eachOwner + " with ownerBlock hash...................");
			Transaction currentTxn = getSignedTrasactionData(eachOwner, ownerBlock.hash);
			ownerAndOwnerBlockTxnMaps.put(eachOwner, currentTxn);

//			System.err.println("TraceCreator, current owner:"+eachOwner);
//			System.out.println("TraceCreator, jobIdToPackIdsMMap:"+ownerToJobIdToPackIdsMMap.get(eachOwner));

			Collection<Multimap<String, Multimap<String, String>>> currentJobIds = ownerToJobIdToPackIdsMMap
					.get(eachOwner);
//			System.err.println("TraceCreator, currentJobIds:"+currentJobIds);
			Iterator<Multimap<String, Multimap<String, String>>> currentJobIdsIterator = currentJobIds.iterator();
			while (currentJobIdsIterator.hasNext()) {
				Multimap<String, Multimap<String, String>> currentjobIdPackIdsMMap = currentJobIdsIterator.next();
				Multimap<String, Multimap<String, String>> hashedJobIdWithPackIdsMMap = HashMultimap.create();
				for (String eachJobId : currentjobIdPackIdsMMap.keySet()) {
					// Job ID connected to owner
					BlockCreator jobIdOwnerBlock = new BlockCreator(eachJobId, ownerBlock.hash);
					jobHashWithIdMap.put(jobIdOwnerBlock.hash, eachJobId);
//					System.out.println("Job ID: " + eachJobId + " hash =" + jobIdOwnerBlock.hash);

					// Pack ID connection to JobId
					Collection<Multimap<String, String>> currentPackIds = currentjobIdPackIdsMMap.get(eachJobId);
					Iterator<Multimap<String, String>> packIdsIterator = currentPackIds.iterator();
					while (packIdsIterator.hasNext()) {
						Multimap<String, String> currentJobPackFileIdsMMap = packIdsIterator.next();
						Multimap<String, String> hashedPackWithFileIdsMMap = HashMultimap.create();
//						System.err.println("currentFileIdsMMap:" + currentJobPackFileIdsMMap);
						// File Id connection to PackId
						for (String eachPackId : currentJobPackFileIdsMMap.keySet()) {
							BlockCreator packIdJobIdBlock = new BlockCreator(eachPackId, jobIdOwnerBlock.hash);
							packHashWithIdMap.put(packIdJobIdBlock.hash, eachPackId);
							Collection<String> currentFileIds = currentJobPackFileIdsMMap.get(eachPackId);
							Iterator<String> currentFileIdsIterator = currentFileIds.iterator();
							while (currentFileIdsIterator.hasNext()) {
								String currentFileId = currentFileIdsIterator.next();
								BlockCreator fileIdPackIdBlock = new BlockCreator(currentFileId, packIdJobIdBlock.hash);
								hashedPackWithFileIdsMMap.put(packIdJobIdBlock.hash, fileIdPackIdBlock.hash);
								fileHashWithIdMap.put(fileIdPackIdBlock.hash, currentFileId);
//								System.err.println("Hash:"+fileIdPackIdBlock.hash+", Id:"+currentFileId);
							}
//							System.err.println("Pack ID: " + currentPackId + " hash =" + packIdJobIdBlock.hash);
							hashedJobIdWithPackIdsMMap.put(jobIdOwnerBlock.hash, hashedPackWithFileIdsMMap);
						}
					}
				}
				// Storing owner->jobIds->packIds->fileIds
				hashedOwnerWithJobIdsMMap.put(ownerBlock.hash, hashedJobIdWithPackIdsMMap);
			}
		}

		logger.debug("Final Hashed Data from DB:" + hashedOwnerWithJobIdsMMap);
	}

	public void publishPublicKeys() {
		for (String eachUser : ownerAndSigningKeysMap.keySet()) {
			logger.info(eachUser + ":" + ownerAndSigningKeysMap.get(eachUser).publicKey);
		}
	}

	public boolean validateUserAccess(String ownerId, PublicKey ownKey) {
		Transaction currTxn = ownerAndOwnerBlockTxnMaps.get(ownerId);
		if (currTxn == null) {
			logger.info("Details for the user:" + ownerId + " does not exist");
			return false;
		} else if (!currTxn.getReciepient().equals(ownKey)) {
			logger.info("For user:" + ownerId + ", Public Key does not match.");
			return false;
		} else {
			if (currTxn.verifySignature()) {
				logger.info("Valid user :" + ownerId + ", with correct Public Key.");
			}
			return true;
		}

	}
	
	public boolean validateUserCreds(String ownerId, PublicKey ownKey,Transaction currTxn) {
		if (!currTxn.getReciepient().equals(ownKey)) {
//			logger.info("For user:" + ownerId + ", Public Key does not match.");
			return false;
		} else {
			if (currTxn.verifySignature()) {
//				logger.info("Valid user :" + ownerId + ", with correct Public Key.");
			}
			return true;
		}

	}
	
	public String getExtractJson(String ownerId, PublicKey ownKey,ArrayList<ArrayList<Transaction>>assoTxns)throws DBException{
		
		for(ArrayList<Transaction> eachTxn:assoTxns) {
			Transaction currTxn=eachTxn.get(eachTxn.size()-1);//Starts with zero, the last element is extract
			String extractJsonHash = currTxn.getDataToBSigned();
			System.err.println(extractJsonHash);
		}
		return null;
	}

	public ArrayList<String> traceBack(String ownerId, PublicKey ownKey) throws DBException {
		ArrayList<String> fileNames = new ArrayList<String>();
		ArrayList<String> fileIds = new ArrayList<String>();
		if (validateUserAccess(ownerId, ownKey)) {
			Transaction currTxn = ownerAndOwnerBlockTxnMaps.get(ownerId);
			String ownerHash = currTxn.getDataToBSigned();
//			System.err.println("Owner hash:" + ownerHash);
			Collection<Multimap<String, Multimap<String, String>>> hashedJobIdWithPackIdsMMap = hashedOwnerWithJobIdsMMap
					.get(ownerHash);
			Iterator<Multimap<String, Multimap<String, String>>> jobIdsItr = hashedJobIdWithPackIdsMMap.iterator();
			while (jobIdsItr.hasNext()) {
				Multimap<String, Multimap<String, String>> jobIdsMMap = jobIdsItr.next();
//				System.out.println("Job Ids->PackIds->FileIds MMap:" + jobIdsMMap);
				for (String eachJobIdHash : jobIdsMMap.keySet()) {
//					System.err.println("Job Ids Hash:" + eachJobIdHash);
					Collection<Multimap<String, String>> packIdsForJobId = jobIdsMMap.get(eachJobIdHash);
					Iterator<Multimap<String, String>> packIdsItr = packIdsForJobId.iterator();
					while (packIdsItr.hasNext()) {
						Multimap<String, String> packIdsMMap = packIdsItr.next();
//						System.out.println("PackIds->FileIds MMap:" + packIdsMMap);
						for (String eachPackIdHash : packIdsMMap.keySet()) {
//							System.err.println("Pack Ids Hash:" + eachPackIdHash);
							Collection<String> fileIdsForPackIds = packIdsMMap.get(eachPackIdHash);
							Iterator<String> fileIdsItr = fileIdsForPackIds.iterator();
							while (fileIdsItr.hasNext()) {
								String eachFileIdHash = fileIdsItr.next();
//								System.err.println("File Ids Hash:" + eachFileIdHash);
//								System.out.println("Actual File ID:" + fileHashWithIdMap.get(eachFileIdHash));
								fileIds.add(fileHashWithIdMap.get(eachFileIdHash));
							}
						}
					}
				}
			}
		}
		fileNames = controller.getFileNames(fileIds);
		return fileNames;
	}

	public ArrayList<ArrayList<Transaction>> createSignedBlocks() throws DBException {
		ArrayList<ArrayList<Transaction>> finalBlockChain=new ArrayList<ArrayList<Transaction>>();
		ArrayList<String> jobOwners = controller.getOwners();
		LinkedHashMap<String, Transaction> ownerTxnMaps = new LinkedHashMap<String, Transaction>();
		for (String eachOwner : jobOwners) {
			logger.info("Creating Blocks for owner:"+eachOwner);
			/*
			 * Generate Public, Private Keys for Owner and populate ownerAndSigningKeysMap
			 * of owner with security keys; Generate Hash for Owner, Transaction for owner
			 * with Hash of owner
			 */
			logger.debug("Generating Security Keys for Owner:" + eachOwner);
			ownerAndSigningKeysMap.put(eachOwner, new SignatureUtil());
			logger.debug("Generating Hash for Owner:" + eachOwner);
			BlockCreator ownerBlock = new BlockCreator(eachOwner, "0");
			logger.debug("Generating Transaction for Owner:" + eachOwner);
			Transaction ownerTxn = getSignedTrasactionData(eachOwner, ownerBlock.hash);
			ownerTxnMaps.put(eachOwner, ownerTxn);

			//Associating each owner with jobids
			ArrayList<String> associatedJobIds = controller.getJobIds(eachOwner);
			LinkedHashMap<String, Transaction> jobIdTxnMaps = new LinkedHashMap<String, Transaction>();
			for (String eachJobId : associatedJobIds) {
				logger.debug("Generating Hash for JobId:" + eachJobId);
				BlockCreator jobIdBlock = new BlockCreator(eachJobId, ownerBlock.hash);
				logger.debug("Generating Transaction for JobId:" + eachJobId);
				Transaction jobIdTxn = getSignedTrasactionData(eachOwner, jobIdBlock.hash);
				jobIdTxnMaps.put(eachJobId, jobIdTxn);
				
				//Associating each jobid with packIds
				ArrayList<String> associatedPackIds =controller.getPackIds(eachJobId);
				LinkedHashMap<String, Transaction> packIdTxnMaps = new LinkedHashMap<String, Transaction>();
//				logger.info(associatedPackIds);
				for(String eachPackId:associatedPackIds) {
					logger.debug("Generating Hash for PackId:" + eachPackId);
					BlockCreator packIdBlock = new BlockCreator(eachPackId, jobIdBlock.hash);
					logger.debug("Generating Transaction for PackId:" + eachPackId);
					Transaction packIdTxn = getSignedTrasactionData(eachOwner, packIdBlock.hash);
					packIdTxnMaps.put(eachPackId, packIdTxn);
					
					//Associating each packId with fileIds
					ArrayList<String> associatedFileIds =controller.getFileIds(eachPackId);
					LinkedHashMap<String, Transaction> fileIdTxnMaps = new LinkedHashMap<String, Transaction>();
//					logger.info(associatedFileIds);
					for(String eachFileId:associatedFileIds) {
						logger.debug("Generating Hash for FileId:" + eachFileId);
						BlockCreator fileIdBlock = new BlockCreator(eachFileId, packIdBlock.hash);
						logger.debug("Generating Transaction for FileId:" + eachFileId);
						Transaction fileIdTxn = getSignedTrasactionData(eachOwner, fileIdBlock.hash);
						fileIdTxnMaps.put(eachFileId, fileIdTxn);
						
						//Associating each fileId with extracts
						ArrayList<String> associatedExtractIds =controller.getExtractIds(eachFileId);
						LinkedHashMap<String, Transaction> extractsIdTxnMaps = new LinkedHashMap<String, Transaction>();
//						logger.info(associatedExtracts.size());
						for(String eachExtractId:associatedExtractIds) {
							logger.debug("Generating Hash for ExtractId:" + eachExtractId);
							BlockCreator extractIdBlock = new BlockCreator(eachExtractId, fileIdBlock.hash);
							logger.debug("Generating Transaction for ExtractId:" + eachExtractId);
							Transaction extractIdTxn = getSignedTrasactionData(eachOwner, extractIdBlock.hash);
							extractsIdTxnMaps.put(eachExtractId, extractIdTxn);	
							
							//Associating each extractJson with extractId
							ArrayList<String> associatedExtracts =controller.getExtracts(eachExtractId);		
							for(String eachExtract:associatedExtracts) {
								ArrayList<Transaction> currentRecord=new ArrayList<Transaction>();
								logger.debug("Generating Hash for Extract:" + eachExtract);
								BlockCreator extractsBlock = new BlockCreator(eachExtract, extractIdBlock.hash);
								logger.debug("Generating Transaction for Extract:" + eachExtract);
								Transaction extractsTxn = getSignedTrasactionData(eachOwner, extractsBlock.hash);
								logger.debug("Transaction for Extract:" + extractsTxn);
								currentRecord.add(ownerTxn);
								currentRecord.add(jobIdTxn);
								currentRecord.add(packIdTxn);
								currentRecord.add(fileIdTxn);	
								currentRecord.add(extractIdTxn);
								currentRecord.add(extractsTxn);
								finalBlockChain.add(currentRecord);
							}
						}
//						logger.info(extractsIdTxnMaps.size());
						
					}
//					logger.info(fileIdTxnMaps);
				}
//				logger.info(packIdTxnMaps);
			}
//			logger.info(jobIdTxnMaps);
		}

//		logger.info(ownerTxnMaps);
//		System.err.println(finalBlockChain);

		logger.info("Completed Blockchain");
		return finalBlockChain;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Environment not metioned. Aborting...");
			System.exit(1);
		}
		String currentEnv = args[0].trim().toLowerCase();

		System.out.println("*******************************************************************************");
		System.out.println("		BlockChain Sumyag Version 1.0.0327");
		System.out.println("*******************************************************************************");
		System.out.println();
		System.out.println("            #########################################");
		System.out.println("			Build Tag 20200327");
		System.out.println("            #########################################");
		System.out.println();
		try {
			TraceCreator tracer = new TraceCreator(currentEnv);
//			logger.info("Initiating Block Chain creation...............");
//			tracer.createSignedBlockChainFileTrace();
//			logger.info("Completed Block Chain creation...............");

//			// Check the public keys
////			tracer.publishPublicKeys();
//
			logger.info("...............Case: Access violation - Wrong public key...............");
			//Change according to what is available in extracts table
			String actualOwnerId = "akash@sumyag.com";
			String faultyOwnerId = "sudip@sumyag.com";
			PublicKey wrongPubKey = tracer.ownerAndSigningKeysMap.get(faultyOwnerId).publicKey;// Pk of sudip
			tracer.validateUserAccess(actualOwnerId, wrongPubKey);
			logger.info("...............Case: Correct public key, same user...............");
			PublicKey correctPubKey = tracer.ownerAndSigningKeysMap.get(actualOwnerId).publicKey;
			tracer.validateUserAccess(actualOwnerId, correctPubKey);

			logger.info("...............Case: Getting file(s) used by a speific user: " + actualOwnerId + "...............");
			ArrayList<String> filePaths = tracer.traceBack(actualOwnerId, correctPubKey);
			for(String eachFilePath:filePaths) {
				logger.info(eachFilePath);
			}
			
			ArrayList<String> jobOwners=tracer.controller.getOwners();
			for(String eachOwner:jobOwners) {
				logger.info(eachOwner);
			}

//			tracer.controller.getDataForChainExtractsTrace();
			
			
//			ArrayList<ArrayList<Transaction>> blockChainRecs=tracer.createSignedBlocks();
//			System.err.println("All records:"+blockChainRecs.size());
//			String testOwner="abhi@sumyag.com";
//			PublicKey correctPubKey = tracer.ownerAndSigningKeysMap.get(testOwner).publicKey;
//			ArrayList<ArrayList<Transaction>> userTxn=new ArrayList<ArrayList<Transaction>>();
//			for(ArrayList<Transaction> eachRec:blockChainRecs) {
//				ArrayList<Transaction> currentTxn=eachRec;
//				Transaction ownerPresent=currentTxn.get(0);
//				if(tracer.validateUserCreds(testOwner, correctPubKey, ownerPresent)) {
//					userTxn.add(eachRec);
//				}
//			}
//			System.err.println("Relevant records:"+userTxn.size());
//			tracer.getExtractJson(testOwner, correctPubKey,userTxn);

		} catch (ConfigException e) {
			e.printStackTrace();
		} catch (DBException e) {
			e.printStackTrace();
		}

	}

}
