/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package sdsl.chainreaction.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Sudip Das
 *
 */
public class PGresDb implements SdslDbInterface {

	@Override
	public Connection getConnection(Properties dbProps) throws SQLException, ClassNotFoundException {

		String driver = dbProps.getProperty("POSTGRES_DRIVER");
		String dbName = dbProps.getProperty("RDS_DB_NAME");
		String userName = dbProps.getProperty("RDS_USERNAME");
		String pwd = dbProps.getProperty("RDS_PASSWORD");
		String hostName = dbProps.getProperty("RDS_HOSTNAME");
		String portNum = dbProps.getProperty("RDS_PORT");

		Class.forName(driver);

		String jdbcUrl = "jdbc:postgresql://" + hostName + ":" + portNum + "/" + dbName + "?user=" + userName
				+ "&password=" + pwd;
		return DriverManager.getConnection(jdbcUrl);
	}
}
