/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package sdsl.chainreaction.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Sudip Das
 *
 */
public class JsonUtil {

	/**
	 * Method to convert a JSON file to String
	 * 
	 * @param classloader ClassLoader for file system
	 * @param jsonFile    JSON file to convert
	 * @return Data from JSON file as String
	 * @throws IOException
	 */
	public static String getJSonToStr(ClassLoader classloader, String jsonFile) throws IOException {
		StringBuffer jsonStrBuff = new StringBuffer();
		InputStream jsonIs = classloader.getResourceAsStream(jsonFile);
		BufferedReader jsonBr = new BufferedReader(new InputStreamReader(jsonIs));
		String readJsonStr = null;
		while ((readJsonStr = jsonBr.readLine()) != null) {
			jsonStrBuff.append(readJsonStr);
		}
		return jsonStrBuff.toString();
	}

	/**
	 * Method to get a list of all top level nodes/keys from a Nested JSON
	 * 
	 * @param jsonStr Data as String from JSON file
	 * @return List of top level nodes/keys
	 * @throws ParseException
	 */
	public static ArrayList<String> get1LevelKeysNestedJson(String jsonStr) throws ParseException {
		ArrayList<String> keysList = new ArrayList<String>();
		Object parsedJSON = new JSONParser().parse(jsonStr);
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		if (firstLevelObj == null) {
			return null;
		}
		Iterator<?> firstLevelIterator = firstLevelObj.entrySet().iterator();
		while (firstLevelIterator.hasNext()) {
			Map.Entry<?, ?> firstLevelNodesMap = (Map.Entry<?, ?>) firstLevelIterator.next();
			keysList.add(firstLevelNodesMap.getKey().toString());
		}
		return keysList;
	}

	/**
	 * Method to get a list of all second level nodes/keys from a Nested JSON
	 * 
	 * @param jsonStr   Data as String from JSON file
	 * @param level1Str First level node/key String for search
	 * @return List of Second level nodes/keys
	 * @throws ParseException
	 */
	public static ArrayList<String> get2LevelKeysNestedJson(String jsonStr, String level1Str) throws ParseException {
		ArrayList<String> keysList = new ArrayList<String>();

		Object parsedJSON = new JSONParser().parse(jsonStr);
		// First level or root level, return null if node does not exist
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		if (firstLevelObj == null) {
			return null;
		}
//		Second level using search String of node name, return null if node does not exist 
		JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(level1Str);
		if (secondLevelObj == null) {
			return null;
		}
		Iterator<?> seondLevelIterator = secondLevelObj.entrySet().iterator();
		while (seondLevelIterator.hasNext()) {
			Map.Entry<?, ?> secondLevelKeyVals = (Map.Entry<?, ?>) seondLevelIterator.next();
			keysList.add(secondLevelKeyVals.getKey().toString());
		}
		return keysList;
	}

	/**
	 * Method to get a list of all third level nodes/keys from a Nested JSON
	 * 
	 * @param jsonStr   Data as String from JSON file
	 * @param level1Str First level node/key String for search
	 * @param level2Str Second level node/key String for search
	 * @return List of Third level nodes/keys
	 * @throws ParseException
	 */
	public static ArrayList<String> get3LevelArrayMapsInNestedJson(String jsonStr, String level1Str, String level2Str)
			throws ParseException {
		ArrayList<String> keysList = new ArrayList<String>();

		Object parsedJSON = new JSONParser().parse(jsonStr);
		// First level or root level
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		if (firstLevelObj == null) {
			return null;
		}
//		Second level using search String of node name 
		JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(level1Str);
		if (secondLevelObj == null) {
			return null;
		}
		/*
		 * Check if the JSON object is of type JSONArray before proceeding
		 */
		Object objType=secondLevelObj.get(level2Str);
		if(objType instanceof JSONArray) {
//			Third level Array search String of node name
			JSONArray thirdLevelArray = (JSONArray) secondLevelObj.get(level2Str);
			Iterator<?> thirdLevelIterator = thirdLevelArray.iterator();
			while (thirdLevelIterator.hasNext()) {
				JSONObject arrayElement = (JSONObject) thirdLevelIterator.next();
//				System.err.println(arrayElement);
				Iterator<?> fourthLevelIterator = arrayElement.entrySet().iterator();
				while (fourthLevelIterator.hasNext()) {
					Map.Entry<?, ?> fourthLevelKeyVals = (Map.Entry<?, ?>) fourthLevelIterator.next();
//					System.err.println(
//							fourthLevelKeyVals.getKey().toString() + ":" + fourthLevelKeyVals.getValue().toString());
					if (fourthLevelKeyVals.getKey().toString().equalsIgnoreCase("key")) {
						keysList.add(fourthLevelKeyVals.getValue().toString());
					}
				}
			}
		}
		return keysList;
	}

	/**
	 * Method to check if list of nodes from one JSON contains all the nodes from
	 * another JSON
	 * 
	 * @param configuredJsonStr configured JSON data reference nodes
	 * @param testJsonStr       nodes from test JSON data to be validated
	 * @return true if valid
	 * @throws ParseException
	 */
	public static boolean isConfiguredQuestion(String configuredJsonStr, String testJsonStr) throws ParseException {
		ArrayList<String> configuredNodes = JsonUtil.get1LevelKeysNestedJson(configuredJsonStr);
		ArrayList<String> testNodes = JsonUtil.get1LevelKeysNestedJson(testJsonStr);
		System.out.println("Confugured Nodes:" + configuredNodes);
		System.out.println("test Nodes:" + testNodes);
		return JsonUtil.get1LevelKeysNestedJson(configuredJsonStr)
				.containsAll(JsonUtil.get1LevelKeysNestedJson(testJsonStr)) ? true : false;
	}

	/**
	 * Method to create a HashMap of HashMaps from a nested JSON using node name in
	 * first level as search node. Outer HashMap has the first level search node as
	 * key and the key value pairs from the second node form the inner HashMap.
	 * 
	 * @param jsonStr Data as String from a JSON file
	 * @return Map of maps
	 * @throws ParseException
	 */

	/*
	 * Example:Used for Content JSON: One node { "CTDEFAULT": { "contentlabel":
	 * "Current term content(s):", "pagelabel": "Current term:" }, "PTDEFAULT": {
	 * "contentlabel": "Reference term content(s):", "pagelabel": "Reference term:"
	 * } }
	 */
	// public static HashMap<String, HashMap<String, String>>
	// getMapOfMapsFromNestedJson(String jsonStr)
	public static HashMap<String, HashMap<String, String>> getDoubleNestedMapsFromDoubleNestedJson(String jsonStr)
			throws ParseException {
		Object parsedJSON = new JSONParser().parse(jsonStr);
		JSONObject firstLevelObj = (JSONObject) parsedJSON;

//		first level node as key and HashMap of second level mapping
		HashMap<String, HashMap<String, String>> firstLevelMap = new HashMap<String, HashMap<String, String>>();
		Iterator<?> secondLevelIterator = firstLevelObj.entrySet().iterator();
		while (secondLevelIterator.hasNext()) {
			Map.Entry<?, ?> secondLevelNodes = (Map.Entry<?, ?>) secondLevelIterator.next();
			JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(secondLevelNodes.getKey());
			Iterator<?> keyValIterator = secondLevelObj.entrySet().iterator();

//			Second level node as key and values
			HashMap<String, String> secondLevelMap = new HashMap<String, String>();
			while (keyValIterator.hasNext()) {
				Map.Entry<?, ?> keyVals = (Map.Entry<?, ?>) keyValIterator.next();
				secondLevelMap.put(keyVals.getKey().toString(), keyVals.getValue().toString());
			}
			firstLevelMap.put(secondLevelNodes.getKey().toString(), secondLevelMap);
		}
		return firstLevelMap;
	}

	/**
	 * Method to create a HashMap of HashMaps from a nested JSON using node name in
	 * second level as search node. Outer HashMap has the second level search node
	 * as key and within the node the key value pairs from the third node form the
	 * inner HashMap. The first level nodes are ignored.
	 * 
	 * @param jsonStr Data as String from JSON file
	 * @param nodeStr second level JSON Node to search within the String
	 * @return Map of second level node as keys with Map of third level values as
	 *         key value pairs
	 * @throws ParseException
	 */
	/*
	 * Example: Used for checklist JSON: Nodes within nodes-2 levels {
	 * "singlerecord": { "UC1": { "mnemonic": "Correct Underwriting Company?",
	 * "lob": "all", "qsthreshold": 50.0, "binarymatch": false, "lobdependency":
	 * false, "descmatch": false, "descthreshold": 0.0 }, "PP1": { "mnemonic":
	 * "Correct Policy Period?", "lob": "all", "qsthreshold": 0.0, "binarymatch":
	 * true, "lobdependency": false, "descmatch": false, "descthreshold": 0.0 }, } }
	 * singlerecord is ignored
	 */
//public static HashMap<String, HashMap<String, String>> getMapOfMapsFromNestedJsonMultiNode(String jsonStr,String nodeStr)
	public static HashMap<String, HashMap<String, String>> getDoubleNestedMapsFromTripleNestedJsonPerNode(
			String jsonStr, String nodeStr) throws ParseException {
		Object parsedJSON = new JSONParser().parse(jsonStr);
		JSONObject parsedJsonObj = (JSONObject) parsedJSON;

		JSONObject firstLevelObj = (JSONObject) parsedJsonObj.get(nodeStr);
		Iterator<?> secondLevelIterator = firstLevelObj.entrySet().iterator();

//		second level node as key and HashMap of third level mapping
		HashMap<String, HashMap<String, String>> secondLevelMap = new HashMap<String, HashMap<String, String>>();
		while (secondLevelIterator.hasNext()) {
			Map.Entry<?, ?> secondLevelNodes = (Map.Entry<?, ?>) secondLevelIterator.next();
			JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(secondLevelNodes.getKey());
			Iterator<?> thirdLevelIterator = secondLevelObj.entrySet().iterator();

//			Third level node as key and values 
			HashMap<String, String> thirdLevelMap = new HashMap<String, String>();
			while (thirdLevelIterator.hasNext()) {
				Map.Entry<?, ?> keyVals = (Map.Entry<?, ?>) thirdLevelIterator.next();
				thirdLevelMap.put(keyVals.getKey().toString(), keyVals.getValue().toString());
			}
			secondLevelMap.put(secondLevelNodes.getKey().toString(), thirdLevelMap);
		}
		return secondLevelMap;
	}

	/**
	 * Method to create a HashMap of HashMaps from a nested JSON. Outermost HashMap
	 * uses first level node as key. Inner HashMap inside Outer HashMap uses second
	 * level node as key. Innermost HashMap uses the key value pairs from the third
	 * node.
	 * 
	 * @param jsonStr Data as String from JSON file
	 * @param nodeStr first level JSON Node to search within the String
	 * @return Map of first level node as key holding Map with second level node as
	 *         key holding Map of third level values as key value pairs
	 * @throws ParseException
	 */
	public static HashMap<String, HashMap<String, HashMap<String, String>>> getTripleNestedMapsFromTripleNestedJsonPerNode(
			String jsonStr, String nodeStr) throws ParseException {
		Object parsedJSON = new JSONParser().parse(jsonStr);
		JSONObject parsedJsonObj = (JSONObject) parsedJSON;

//		first level node as key and HashMap of second level containing HashMap of third level mapping
		HashMap<String, HashMap<String, HashMap<String, String>>> firstLevelMap = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		JSONObject firstLevelObj = (JSONObject) parsedJsonObj.get(nodeStr);
		Iterator<?> secondLevelIterator = firstLevelObj.entrySet().iterator();

//		second level node as key and HashMap of third level mapping
		HashMap<String, HashMap<String, String>> secondLevelMap = new HashMap<String, HashMap<String, String>>();
		while (secondLevelIterator.hasNext()) {
			Map.Entry<?, ?> secondLevelKeysMap = (Map.Entry<?, ?>) secondLevelIterator.next();
			JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(secondLevelKeysMap.getKey());
			Iterator<?> thirdLevelIterator = secondLevelObj.entrySet().iterator();

//			Third level node as key and values 
			HashMap<String, String> thirdLevelMap = new HashMap<String, String>();
			while (thirdLevelIterator.hasNext()) {
				Map.Entry<?, ?> keyVals = (Map.Entry<?, ?>) thirdLevelIterator.next();
				thirdLevelMap.put(keyVals.getKey().toString(), keyVals.getValue().toString());
			}
			secondLevelMap.put(secondLevelKeysMap.getKey().toString(), thirdLevelMap);
		}
		firstLevelMap.put(nodeStr, secondLevelMap);
		return firstLevelMap;
	}

	public static LinkedHashMap<String, String> get3LevelJsonObjAsStr(String jsonStr, String level1Str,
			String level2Str) throws ParseException {
		ArrayList<String> keysList = new ArrayList<String>();
		LinkedHashMap<String, String> node3relatedJSON = new LinkedHashMap<String, String>();

		Object parsedJSON = new JSONParser().parse(jsonStr);
		// First level or root level
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		if (firstLevelObj == null) {
			return null;
		}
//		Second level using search String of node name 
		JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(level1Str);
		if (secondLevelObj == null) {
			return null;
		}
//		Third level using search String of node name
		JSONObject thirdLevelObj = (JSONObject) secondLevelObj.get(level2Str);
		if (thirdLevelObj == null) {
			return null;
		}
		Iterator<?> thirdLevelIterator = thirdLevelObj.entrySet().iterator();
		while (thirdLevelIterator.hasNext()) {
			Map.Entry<?, ?> thirdLevelKeyVals = (Map.Entry<?, ?>) thirdLevelIterator.next();
			keysList.add(thirdLevelKeyVals.getKey().toString());
			node3relatedJSON.put(level2Str, String.valueOf(thirdLevelObj));
		}
		return node3relatedJSON;
	}

	/**
	 * Create HashMap from JSON that has few one level nested and few non nested
	 * data
	 * 
	 * @param jsonStr JSON data to parse
	 * @return HashMap with key as String and value as Object
	 * @throws ParseException
	 */
	public static HashMap<String, Object> getMixedDBJsonMap(String jsonStr) throws ParseException {
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		Object parsedJSON = new JSONParser().parse(jsonStr);
		// First level or root level
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		Iterator<?> secondLevelIterator = firstLevelObj.entrySet().iterator();
		while (secondLevelIterator.hasNext()) {
			Map.Entry<?, ?> secondLevelKeysMap = (Map.Entry<?, ?>) secondLevelIterator.next();
			// The check is below to see if it is still a nested JSON object or not
			if (firstLevelObj.get(secondLevelKeysMap.getKey()) instanceof JSONObject) {
				// With Second Level
				JSONObject secondLevelObj = (JSONObject) firstLevelObj.get(secondLevelKeysMap.getKey());
				Iterator<?> thirdLevelIterator = secondLevelObj.entrySet().iterator();
				HashMap<String, String> thirdLevelMap = new HashMap<String, String>();
				while (thirdLevelIterator.hasNext()) {
					Map.Entry<?, ?> keyVals = (Map.Entry<?, ?>) thirdLevelIterator.next();
					thirdLevelMap.put(keyVals.getKey().toString(), keyVals.getValue().toString());
				}
				returnMap.put(secondLevelKeysMap.getKey().toString(), thirdLevelMap);

			} else {
				// No second level
				returnMap.put(secondLevelKeysMap.getKey().toString(), secondLevelKeysMap.getValue().toString());
			}

		}
		return returnMap;
	}

	public static boolean isEmptyJSON(String jsonStr) throws ParseException {
		Object parsedJSON = new JSONParser().parse(jsonStr);
		// First level or root level
		JSONObject firstLevelObj = (JSONObject) parsedJSON;
		return firstLevelObj.isEmpty();
	}

	public static void main(String args[]) {
		String jsonFile = "exdionChecklistTemplate.json";
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		try {
			String jsonStr = getJSonToStr(classloader, jsonFile);
			ArrayList<String> qsNodesFromTemplate = get1LevelKeysNestedJson(jsonStr);
			for (String eachQsNode : qsNodesFromTemplate) {
				HashMap<String, HashMap<String, HashMap<String, String>>> allQsTemplateMap = getTripleNestedMapsFromTripleNestedJsonPerNode(
						jsonStr, eachQsNode);
				System.out.println(allQsTemplateMap);
			}
//			System.out.println(getDoubleNestedMapsFromDoubleNestedJson(jsonStr));
//			jsonStr = getJSonToStr(classloader, "exdionContent.json");
//			System.out.println(getDoubleNestedMapsFromDoubleNestedJson(jsonStr));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
