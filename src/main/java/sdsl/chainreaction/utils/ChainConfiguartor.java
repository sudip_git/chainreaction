/**
 * 
 */
package sdsl.chainreaction.utils;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeFieldType;
import org.joda.time.Instant;
import org.json.simple.parser.ParseException;

import sdsl.chainreaction.exceptions.ConfigException;


/**
 * @author Sudip Das
 *
 */
public class ChainConfiguartor {
	/**
	 * Default logger
	 */
	static Logger logger = null;

	public static Properties awsProps = null;
	public static Properties dbProps = null;
	/**
	 * Location of local log files
	 */
	public static String TEMP_LOG_DIR = "./chainreaction_logs/";

	private static String INFRA_JSON = "exdionInfra.json";
	
	public static void init(String currentEnv) throws ConfigException {
		String logFileName = TEMP_LOG_DIR + currentEnv.toUpperCase() + "_" + getLoggerPrefix() + "_ChainR.log";
		System.setProperty("logfile.name", logFileName);
		logger = Logger.getLogger(ChainConfiguartor.class.getName());
		try {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			String jsonStr = JsonUtil.getJSonToStr(classloader, INFRA_JSON);
			awsProps = getInfraProps(jsonStr, "aws");
			dbProps = getInfraProps(jsonStr, currentEnv.toLowerCase());

			if (awsProps == null) {
				throw new ConfigException("Cloud Configuration Parameters not available, contact Support.");
			}
			if (dbProps == null) {
				throw new ConfigException("Wrong Environment Parameters provided: \"" + currentEnv + "\"");
			}
		} catch (IOException e) {
			logger.error("ERROR: IOException while converting JSON to String or loading Input Stream.");
			throw new ConfigException("In init()", e);
		} catch (ParseException e) {
			logger.error("ERROR: ParseException while converting Nested JSON to HashMap of HashMaps / Bin Maps.");
			throw new ConfigException("In init()", e);
		}
	}


	private static String getLoggerPrefix() {
		Instant instantFromTimestamp = new Instant(System.currentTimeMillis());
		StringBuffer buf = new StringBuffer();
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.year())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.monthOfYear())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.dayOfMonth())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.hourOfDay())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.minuteOfDay())));
		buf.append(String.valueOf(instantFromTimestamp.get(DateTimeFieldType.secondOfDay())));
		return buf.toString();
	}

	private static Properties getInfraProps(String jsonStr, String nodeStr) throws ParseException {
		return PropsUtil.getPropsForJsonNode(jsonStr, nodeStr);
	}
}
