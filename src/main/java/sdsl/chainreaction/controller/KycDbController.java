/**
 * 
 */
package sdsl.chainreaction.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.fasterxml.uuid.Generators;

import sdsl.chainreaction.db.KycDb;
import sdsl.chainreaction.exceptions.DBException;
import sdsl.chainreaction.utils.KycConfiguartor;

/**
 * @author Sudip Das
 *
 */
public class KycDbController {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(KycDbController.class.getName());

	private String kycViewName = "kycview";

	/**
	 * Prepared Statements
	 */
	private PreparedStatement getUsersPS = null;
	private PreparedStatement getJobIdsForUserPS = null;
	private PreparedStatement getFileIdsForJobIdPS = null;
	private PreparedStatement getExtractIdsForFileIdPS = null;
	private PreparedStatement getExtractJsonsForExtractIdPS = null;
	private PreparedStatement deleteKycHashPs = null;
	private PreparedStatement insertKycHashPs = null;
	private PreparedStatement deleteKycRetrievalPs = null;
	private PreparedStatement insertKycRetrievalPs = null;
	
	private PreparedStatement retrieveFileNamePS = null;
	
	private PreparedStatement getFileIdHashPS = null;

	/**
	 * Connection
	 */
	private Connection conn = null;

	public KycDbController() throws DBException {
		getConnection();
	}

	private void controller_initialize() throws DBException {
		try {
			String sqlQuery = "select distinct(user_email) from " + kycViewName;
			getUsersPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select distinct(job_id) from " + kycViewName + " where user_email=?";
			getJobIdsForUserPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select distinct(file_id) from " + kycViewName + " where job_id =?";
			getFileIdsForJobIdPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select distinct(id) FROM " + kycViewName + " where file_id=?";
			getExtractIdsForFileIdPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "select sys_json,file_path,file_name FROM " + kycViewName + " where id=?";
			getExtractJsonsForExtractIdPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "DELETE FROM kychash WHERE EXISTS (select *  from kychash)";
			deleteKycHashPs = conn.prepareStatement(sqlQuery);

			sqlQuery = "INSERT INTO kychash VALUES(?,?,?,?,?,?,?)";
			insertKycHashPs = conn.prepareStatement(sqlQuery);

			sqlQuery = "DELETE FROM kycretrieval WHERE EXISTS (select *  from kycretrieval)";
			deleteKycRetrievalPs = conn.prepareStatement(sqlQuery);

			sqlQuery = "INSERT INTO kycretrieval VALUES(?,?,?,?,?)";
			insertKycRetrievalPs = conn.prepareStatement(sqlQuery);
			
			sqlQuery = "select filename FROM kycretrieval where fileid_hash=?";
			retrieveFileNamePS= conn.prepareStatement(sqlQuery);
			
			sqlQuery = "select distinct(fileid_hash) FROM kychash where owner_hash=?";
			getFileIdHashPS= conn.prepareStatement(sqlQuery);
			/*
			 * Creating the view
			 */
			createView();

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller initilization.");
			throw new DBException("In controller_initialize()", e);
		}
	}

	public void controller_finalize() throws DBException {
		try {
			if (getUsersPS != null) {
				getUsersPS.close();
				getUsersPS = null;
			}
			if (getJobIdsForUserPS != null) {
				getJobIdsForUserPS.close();
				getJobIdsForUserPS = null;
			}
			if (getFileIdsForJobIdPS != null) {
				getFileIdsForJobIdPS.close();
				getFileIdsForJobIdPS = null;
			}
			if (getExtractIdsForFileIdPS != null) {
				getExtractIdsForFileIdPS.close();
				getExtractIdsForFileIdPS = null;
			}
			if (getExtractJsonsForExtractIdPS != null) {
				getExtractJsonsForExtractIdPS.close();
				getExtractJsonsForExtractIdPS = null;
			}
			if (deleteKycHashPs != null) {
				deleteKycHashPs.close();
				deleteKycHashPs = null;
			}
			if (insertKycHashPs != null) {
				insertKycHashPs.close();
				insertKycHashPs = null;
			}
			if (deleteKycRetrievalPs != null) {
				deleteKycRetrievalPs.close();
				deleteKycRetrievalPs = null;
			}
			if (insertKycRetrievalPs != null) {
				insertKycRetrievalPs.close();
				insertKycRetrievalPs = null;
			}
			if (retrieveFileNamePS != null) {
				retrieveFileNamePS.close();
				retrieveFileNamePS = null;
			}
			if (getFileIdHashPS != null) {
				getFileIdHashPS.close();
				getFileIdHashPS = null;
			}


			dropView(kycViewName);

			if (conn != null && !conn.isClosed()) {
				if (!conn.getAutoCommit()) {
					conn.commit();
					conn.setAutoCommit(true);
				}
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In controller_finalize()", e);
		}
	}

	/**
	 * Method to create a connection to DB
	 * 
	 * @return Connection to DB
	 * @throws ComparatorException Comparator Exception
	 * @throws DBException         DB Exception
	 */
	private Connection getConnection() throws DBException {
		try {
			logger.info("Connecting to Cloud DB instance.");

			KycDb kycPostgresDB = new KycDb();
			conn = kycPostgresDB.getConnection(KycConfiguartor.dbProps);

			controller_initialize();
			return conn;
		} catch (ClassNotFoundException e) {
			logger.fatal("ERROR: ClassNotFoundException in establising Cloud DB connection");
			throw new DBException("In getConnection()", e);
		} catch (SQLException e) {
			logger.fatal("ERROR: SQLException in establising Cloud DB connection.");
			throw new DBException("In getConnection()", e);
		}
	}

	/**
	 * Method to create a view
	 * 
	 * @throws DBException DB Exception
	 */
	private void createView() throws DBException {
		try {
// LEFT Join introduced instead of INNER JOIN for packs not in file_metadata

			String sqlQuery = "CREATE OR REPLACE view " + kycViewName + " as \n"
					+ "select extracts.*, files.parent_id as job_id, files.name as file_name,files.path as file_path, files.type as file_type, files.status as file_status, \n"
					+ "jobs.name As job_purpose, jobs.status as job_status, jobs.owner as user_id, \n"
					+ "users.email as user_email, users.first_name as user_firstname,users.last_name as user_lastname, \n"
					+ "users.user_status as user_verification_status from extracts \n"
					+ "inner join files on files.id=extracts.file_id\n" + "INNER JOIN jobs ON jobs.id=files.parent_id\n"
					+ "INNER JOIN users ON jobs.\"owner\"=users.user_id";

			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlQuery);
			logger.debug("KYC View: " + kycViewName);

			if (stmt != null) {
				stmt.close();
				stmt = null;
			}

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in creating views.");
			throw new DBException("In createView()", e);
		}
	}

	/**
	 * Method to drop a view
	 * 
	 * @param viewName View Name
	 * @throws DBException
	 */
	private void dropView(String viewName) throws DBException {
		String sqlQuery = "DROP VIEW IF EXISTS " + viewName;
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlQuery);

			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
		} catch (SQLException e) {
			throw new DBException("In dropView()", e);
		}
	}

	public ArrayList<String> getOwners() throws DBException {
		ArrayList<String> jobOwners = new ArrayList<String>();
		try {
			ResultSet ownersResultSet = getUsersPS.executeQuery();
			while (ownersResultSet.next()) {
				jobOwners.add(ownersResultSet.getString("user_email"));
			}

			if (ownersResultSet != null) {
				ownersResultSet.close();
				ownersResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching job_owner.");
			throw new DBException("In getOwners", e);
		}
		return jobOwners;

	}

	public ArrayList<String> getJobIds(String jobOwner) throws DBException {
		ArrayList<String> jobIds = new ArrayList<String>();
		try {
			getJobIdsForUserPS.setString(1, jobOwner);
			ResultSet jobIdsResultSet = getJobIdsForUserPS.executeQuery();
			while (jobIdsResultSet.next()) {
				jobIds.add(jobIdsResultSet.getString("job_id"));
			}

			if (jobIdsResultSet != null) {
				jobIdsResultSet.close();
				jobIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching job_id.");
			throw new DBException("In getJobIds", e);
		}
		return jobIds;

	}

	public ArrayList<String> getFileIds(String job_id) throws DBException {
		ArrayList<String> fileIds = new ArrayList<String>();
		try {
			getFileIdsForJobIdPS.setString(1, job_id);
			ResultSet fileIdsResultSet = getFileIdsForJobIdPS.executeQuery();
			while (fileIdsResultSet.next()) {
				fileIds.add(fileIdsResultSet.getString("file_id"));
			}

			if (fileIdsResultSet != null) {
				fileIdsResultSet.close();
				fileIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching file_id.");
			throw new DBException("In getFileIds", e);
		}
		return fileIds;

	}

	public ArrayList<String> getExtractIds(String fileId) throws DBException {
		ArrayList<String> extractIds = new ArrayList<String>();
		try {
			getExtractIdsForFileIdPS.setString(1, fileId);
			ResultSet extractIdsResultSet = getExtractIdsForFileIdPS.executeQuery();
			while (extractIdsResultSet.next()) {
				extractIds.add(extractIdsResultSet.getString("id"));
			}

			if (extractIdsResultSet != null) {
				extractIdsResultSet.close();
				extractIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching id extraction.");
			throw new DBException("In getExtractIds", e);
		}
		return extractIds;

	}

	public ArrayList<LinkedHashMap<String, String>> getExtracts(String extractId) throws DBException {
//		ArrayList<String> extracts = new ArrayList<String>();
		ArrayList<LinkedHashMap<String, String>> extracts = new ArrayList<LinkedHashMap<String, String>>();

		try {
			getExtractJsonsForExtractIdPS.setString(1, extractId);
			ResultSet extractsResultSet = getExtractJsonsForExtractIdPS.executeQuery();
			while (extractsResultSet.next()) {
//				extracts.add(extractsResultSet.getString("sys_json"));
				LinkedHashMap<String, String> currMap = new LinkedHashMap<String, String>();
				currMap.put("sys_json", extractsResultSet.getString("sys_json"));
				currMap.put("file_path", extractsResultSet.getString("file_path"));
				currMap.put("file_name", extractsResultSet.getString("file_name"));
				extracts.add(currMap);
			}
			if (extractsResultSet != null) {
				extractsResultSet.close();
				extractsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching sys_json.");
			throw new DBException("In getExtracts", e);
		}
		return extracts;

	}

	public void insertHashRecords(ArrayList<LinkedHashMap<String, Object>> dbRows) throws DBException {

		try {
			// Delete existing records.
			logger.debug("Deleting records in kychash");
			deleteKycHashPs.executeUpdate();

			logger.info("Inserting records in kychash");
			final int batchSize = 100000;
			int count = 0;
			conn.setAutoCommit(false);
			for (LinkedHashMap<String, Object> eachRec : dbRows) {
				insertKycHashPs.setObject(1, Generators.timeBasedGenerator().generate(), java.sql.Types.OTHER);
				insertKycHashPs.setString(2, eachRec.get("owner_hash").toString());
				insertKycHashPs.setString(3, eachRec.get("jobid_hash").toString());
				insertKycHashPs.setString(4, eachRec.get("fileid_hash").toString());
				insertKycHashPs.setString(5, eachRec.get("sysjson_hash").toString());
				insertKycHashPs.setString(6, eachRec.get("filepath_hash").toString());
				insertKycHashPs.setString(7, eachRec.get("filename_hash").toString());

				insertKycHashPs.addBatch();
				if (++count % batchSize == 0) {
					logger.info("KycHash count=" + count + ",batchSize=" + batchSize);
					insertKycHashPs.executeBatch();
					conn.commit();
				}
			}
			insertKycHashPs.executeBatch();// Remaining records
			conn.commit();
			logger.info("Completed updating KycHash");

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in record deletion/insertion.");
			throw new DBException("In insertHashRecords", e);
		}

	}

	public void insertRetrievalRecords(ArrayList<LinkedHashMap<String, Object>> dbRows) throws DBException {

		try {
			// Delete existing records.
			logger.debug("Deleting records in kycretrieval");
			deleteKycRetrievalPs.executeUpdate();

			logger.info("Inserting records in kychash");
			final int batchSize = 100000;
			int count = 0;
			conn.setAutoCommit(false);
			for (LinkedHashMap<String, Object> eachRec : dbRows) {
				insertKycRetrievalPs.setObject(1, Generators.timeBasedGenerator().generate(), java.sql.Types.OTHER);
				insertKycRetrievalPs.setString(2, eachRec.get("fileid_hash").toString());
				insertKycRetrievalPs.setString(3, eachRec.get("sysjson").toString());
				insertKycRetrievalPs.setString(4, eachRec.get("filepath").toString());
				insertKycRetrievalPs.setString(5, eachRec.get("filename").toString());

				insertKycRetrievalPs.addBatch();
				if (++count % batchSize == 0) {
					logger.info("kycretrieval count=" + count + ",batchSize=" + batchSize);
					insertKycRetrievalPs.executeBatch();
					conn.commit();
				}
			}
			insertKycRetrievalPs.executeBatch();// Remaining records
			conn.commit();
			logger.info("Completed updating kycretrieval");

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in record deletion/insertion.");
			throw new DBException("In insertRetrievalRecords", e);
		}
	}
	//
	//getFileIdHashPS
	public ArrayList<String> getFileIdHashForOwnerHash(String ownerHashString) throws DBException {
		ArrayList<String> fileIdsHash = new ArrayList<String>();
		try {
			getFileIdHashPS.setString(1, ownerHashString);
			ResultSet fileIdsResultSet = getFileIdHashPS.executeQuery();
			while (fileIdsResultSet.next()) {
				fileIdsHash.add(fileIdsResultSet.getString("fileid_hash"));
			}

			if (fileIdsResultSet != null) {
				fileIdsResultSet.close();
				fileIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching file_names.");
			throw new DBException("In getFileIdHashForOwnerHash", e);
		}
		return fileIdsHash;
	}
	
	
//	retrieveFileNamePS
	public ArrayList<String> getFileNameFromFileIdHash(String fileIdHashString) throws DBException {
		ArrayList<String> fileNames = new ArrayList<String>();
		try {
			retrieveFileNamePS.setString(1, fileIdHashString);
			ResultSet fileIdsResultSet = retrieveFileNamePS.executeQuery();
			while (fileIdsResultSet.next()) {
				fileNames.add(fileIdsResultSet.getString("filename"));
			}

			if (fileIdsResultSet != null) {
				fileIdsResultSet.close();
				fileIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching file names.");
			throw new DBException("In getFileNameFromFileIdHash", e);
		}
		return fileNames;
	}
}
