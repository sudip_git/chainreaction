/**
 * 
 */
package sdsl.chainreaction.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import sdsl.chainreaction.db.PGresDb;
import sdsl.chainreaction.exceptions.DBException;
import sdsl.chainreaction.utils.ChainConfiguartor;

/**
 * @author Sudip Das
 *
 */
public class DbController {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(DbController.class.getName());

	/**
	 * Connection
	 */
	private Connection conn = null;

	/**
	 * Prepared Statements
	 */
	private PreparedStatement getDistinctJobOwnersPS = null;
	private PreparedStatement getJobIdsForOwnerPS = null;
	private PreparedStatement getPackIdsForJobIdPS = null;
	private PreparedStatement getFileIdsForPackIdPS = null;
	private PreparedStatement getExtractIdsForFileIdPS = null;
	private PreparedStatement getExtractJsonsForExtractIdPS = null;
//	private PreparedStatement getJobPackOwnerPS = null;

	public DbController() throws DBException {
		getConnection();
	}

	private void controller_initialize() throws DBException {
		try {
//			String sqlQuery = "select distinct(job_owner) from jobs where job_id in (select job_id from trace)";
			String sqlQuery = "select distinct(job_owner) from jobs where job_id in (select job_id from extracts)";
			getDistinctJobOwnersPS = conn.prepareStatement(sqlQuery);

//			sqlQuery = "select job_id from jobs where job_id in (select job_id from trace) and job_owner=?";
			sqlQuery = "select job_id from jobs where job_id in (select job_id from extracts) and job_owner=?";
			getJobIdsForOwnerPS = conn.prepareStatement(sqlQuery);

//			sqlQuery = "select distinct(pack_id) from files where job_id =?";
			sqlQuery = "select distinct(pack_id) from extracts where job_id =?";
			getPackIdsForJobIdPS = conn.prepareStatement(sqlQuery);

//			sqlQuery = "select distinct(file_id) from files where pack_id =?";
			sqlQuery = "select distinct(file_id) from extracts where pack_id =?";
			getFileIdsForPackIdPS = conn.prepareStatement(sqlQuery);
			
			sqlQuery = "select extr_id FROM extracts where file_id=?";
			getExtractIdsForFileIdPS= conn.prepareStatement(sqlQuery);

//			sqlQuery = "select extr_json FROM extracts where file_id=?";
			sqlQuery = "select extr_json FROM extracts where extr_id=?";
			getExtractJsonsForExtractIdPS = conn.prepareStatement(sqlQuery);

//			sqlQuery = "select jobs.job_id,files.pack_id, jobs.job_owner from jobs,files "
//					+ "where jobs.job_id in (select job_id from trace order by trace_ts desc) and "
//					+ "files.job_id=jobs.job_id";
//			getJobPackOwnerPS = conn.prepareStatement(sqlQuery);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller initilization.");
			throw new DBException("In controller_initialize()", e);
		}
	}

	public void controller_finalize() throws DBException {
		try {
			if (getDistinctJobOwnersPS != null) {
				getDistinctJobOwnersPS.close();
				getDistinctJobOwnersPS = null;
			}
			if (getJobIdsForOwnerPS != null) {
				getJobIdsForOwnerPS.close();
				getJobIdsForOwnerPS = null;
			}
			if (getPackIdsForJobIdPS != null) {
				getPackIdsForJobIdPS.close();
				getPackIdsForJobIdPS = null;
			}
			if (getFileIdsForPackIdPS != null) {
				getFileIdsForPackIdPS.close();
				getFileIdsForPackIdPS = null;
			}
			if (getExtractIdsForFileIdPS != null) {
				getExtractIdsForFileIdPS.close();
				getExtractIdsForFileIdPS = null;
			}
			if (getExtractJsonsForExtractIdPS != null) {
				getExtractJsonsForExtractIdPS.close();
				getExtractJsonsForExtractIdPS = null;
			}
//			if (getJobPackOwnerPS != null) {
//				getJobPackOwnerPS.close();
//				getJobPackOwnerPS = null;
//			}

			if (conn != null && !conn.isClosed()) {
				if (!conn.getAutoCommit()) {
					conn.commit();
					conn.setAutoCommit(true);
				}
				conn.close();
				conn = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In controller_finalize()", e);
		}
	}

	/**
	 * Method to create a connection to DB
	 * 
	 * @return Connection to DB
	 * @throws ComparatorException Comparator Exception
	 * @throws DBException         DB Exception
	 */
	private Connection getConnection() throws DBException {
		try {
			logger.info("Connecting to Cloud DB instance.");

			PGresDb postGresDB = new PGresDb();
			conn = postGresDB.getConnection(ChainConfiguartor.dbProps);

			controller_initialize();
			return conn;
		} catch (ClassNotFoundException e) {
			logger.fatal("ERROR: ClassNotFoundException in establising Cloud DB connection");
			throw new DBException("In getConnection()", e);
		} catch (SQLException e) {
			logger.fatal("ERROR: SQLException in establising Cloud DB connection.");
			throw new DBException("In getConnection()", e);
		}
	}

	public Multimap<String, Multimap<String, Multimap<String, String>>> getDataForChainFilesTrace() throws DBException {
		ArrayList<String> jobOwners = new ArrayList<String>();
		Multimap<String, String> ownerToJobIdsMMap = HashMultimap.create();
		Multimap<String, Multimap<String, Multimap<String, String>>> ownerJobIdPackIdFileIdsMMap = HashMultimap
				.create();
		try {
			ResultSet ownersResultSet = getDistinctJobOwnersPS.executeQuery();
			while (ownersResultSet.next()) {
				jobOwners.add(ownersResultSet.getString("job_owner"));
			}

			if (ownersResultSet != null) {
				ownersResultSet.close();
				ownersResultSet = null;
			}
			logger.info("Job Owners available in current data set:");
			logger.info(jobOwners);
//			System.err.println(jobOwners);

			/*
			 * *********************** COMMENT THESE ***********************
			 */
			jobOwners.clear();
			jobOwners.add("abhi@sumyag.com");
//			jobOwners.add("sudip@sumyag.com");
//			jobOwners.add("akash@sumyag.com");
			/*
			 * *********************** END COMMENT ***********************
			 */

			logger.debug("Getting Block chain data for available owners in extracts.");
			ResultSet jobIdsResultSet = null;
			for (String eachOwner : jobOwners) {
				Multimap<String, Multimap<String, String>> jobIdToPackIdsMMap = HashMultimap.create();
				getJobIdsForOwnerPS.setString(1, eachOwner);
				jobIdsResultSet = getJobIdsForOwnerPS.executeQuery();
				while (jobIdsResultSet.next()) {
					String job_id = jobIdsResultSet.getString("job_id");
//					System.err.println(eachOwner+"="+job_id);
					ownerToJobIdsMMap.put(eachOwner, job_id);
					getPackIdsForJobIdPS.setString(1, job_id);
					ResultSet packIdsResultSet = getPackIdsForJobIdPS.executeQuery();
					while (packIdsResultSet.next()) {
						Multimap<String, String> packIdToFileIdsMMap = HashMultimap.create();
						String pack_id = packIdsResultSet.getString("pack_id");
//						jobIdToPackIdsMMap.put(job_id, pack_id);
//						System.err.println(job_id+"="+pack_id);
						getFileIdsForPackIdPS.setString(1, pack_id);
						ResultSet fileIdsResultSet = getFileIdsForPackIdPS.executeQuery();
						while (fileIdsResultSet.next()) {
							Multimap<String, String> fileIdsToExtractsMMap = HashMultimap.create();
							String file_id = fileIdsResultSet.getString("file_id");
//							System.err.println(job_id+"="+pack_id+"="+file_id);
							getExtractJsonsForExtractIdPS.setString(1, file_id);
							ResultSet extrJsonsResultSet = getExtractJsonsForExtractIdPS.executeQuery();
							while (extrJsonsResultSet.next()) {
								String extr_json = extrJsonsResultSet.getString("extr_json");
								fileIdsToExtractsMMap.put(file_id, extr_json);
							}
							System.err.println(fileIdsToExtractsMMap.size());
							packIdToFileIdsMMap.put(pack_id, file_id);
						}
						jobIdToPackIdsMMap.put(job_id, packIdToFileIdsMMap);
//						System.err.println("packIdToFileIdsMMap="+packIdToFileIdsMMap);

						if (fileIdsResultSet != null) {
							fileIdsResultSet.close();
							fileIdsResultSet = null;
						}
					}

					if (packIdsResultSet != null) {
						packIdsResultSet.close();
						packIdsResultSet = null;
					}
				}
				ownerJobIdPackIdFileIdsMMap.put(eachOwner, jobIdToPackIdsMMap);
			}

			if (jobIdsResultSet != null) {
				jobIdsResultSet.close();
				jobIdsResultSet = null;
			}
			logger.debug("Block chain data received for job owners. Tranferring control for Trace creation.");
//			System.out.println(ownerToJobIdToPackIdsMMap);

			// CHECKING
//			for(String eachOwner:jobOwners) {
//				System.err.println("Db controller, current owner:"+eachOwner);
//				System.out.println("Db controller, jobIdToPackIdsMMap:"+ownerToJobIdToPackIdsMMap.get(eachOwner));
//				Collection<Multimap<String, String>> currentJobIds=ownerToJobIdToPackIdsMMap.get(eachOwner);
//				System.err.println("Db controller, currentJobIds:"+currentJobIds);
//				Iterator<Multimap<String, String>> jobIdsIterator=currentJobIds.iterator();
//				while(jobIdsIterator.hasNext()) {
//					Multimap<String, String> currentjobIdPackIdsMMap=jobIdsIterator.next();
//					for(String eachJobId:currentjobIdPackIdsMMap.keySet() ) {
//						Collection <String> currentPackIds=currentjobIdPackIdsMMap.get(eachJobId);
//						Iterator <String>packIdsIterator=currentPackIds.iterator();
//						while(packIdsIterator.hasNext()) {
//							String currentPackId=packIdsIterator.next();
//							System.out.println("Db controller currentPackId:"+currentPackId);
//						}
//					}
//				}
//				try {
//					Thread.sleep(50);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}

			// NOT BEING USED
//			ResultSet packIdsResultSet=null;
//			for(String eachJobIdPerOwner:ownerToJobIdsMMap.keySet()) {
//				Iterator<String> currentIterator=ownerToJobIdsMMap.get(eachJobIdPerOwner).iterator();
//				while(currentIterator.hasNext()) {
//					String currentJobId=currentIterator.next();
//					getPackIdsForJobIdPS.setString(1, currentJobId);
//					packIdsResultSet=getPackIdsForJobIdPS.executeQuery();
//					while (packIdsResultSet.next()) {
//						jobIdToPackIdsMMap.put(currentJobId, packIdsResultSet.getString("pack_id"));
////						System.out.println(eachOwner+":"+currentJobId+"->"+resultSet.getString("pack_id"));
//					}
//				}
//				ownerToJobIdToPackIdsMMap.put(eachJobIdPerOwner, jobIdToPackIdsMMap);
//			}
////			System.err.println(ownerToJobIdToPackIdsMMap);
//
//			if (packIdsResultSet != null) {
//				packIdsResultSet.close();
//				packIdsResultSet = null;
//			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching job_id, pack_id, job_owner.");
			throw new DBException("In getDataForChain", e);
		}
		return ownerJobIdPackIdFileIdsMMap;
	}

	/**
	 * @param fileIds
	 * @throws DBException
	 */
	public ArrayList<String> getFileNames(ArrayList<String> fileIds) throws DBException {
		ArrayList<String> fileNames = new ArrayList<String>();
		String fileIdsList = String.join("','", fileIds);
		fileIdsList = "'" + fileIdsList + "'";
		try {
			String sqlQuery = "select file_path from files where file_id in (" + fileIdsList + ")";
			Statement stmt = conn.createStatement();
			ResultSet resultset = stmt.executeQuery(sqlQuery);
			while (resultset.next()) {
				String file_path = resultset.getString("file_path");
				fileNames.add(file_path);

			}
			if (resultset != null) {
				resultset.close();
				resultset = null;
			}
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching file_paths.");
			throw new DBException("In getFileNames", e);
		}
		return fileNames;
	}

	public void getDataForChainExtractsTrace() throws DBException {
		ArrayList<String> jobOwners = getOwners();

		for (String eachOwner : jobOwners) {
			ArrayList<String> jobIds = getJobIds(eachOwner);
			System.err.println(eachOwner + ":" + jobIds);
		}

	}

	public ArrayList<String> getOwners() throws DBException {
		ArrayList<String> jobOwners = new ArrayList<String>();
		try {
			ResultSet ownersResultSet = getDistinctJobOwnersPS.executeQuery();
			while (ownersResultSet.next()) {
				jobOwners.add(ownersResultSet.getString("job_owner"));
			}

			if (ownersResultSet != null) {
				ownersResultSet.close();
				ownersResultSet = null;
			}
			
			//REMOVE THE FOLLOWING LINES ***********************************************************************************
			if(jobOwners.size()>2) {
				String currentOwner1=jobOwners.get(0);
				String currentOwner2=jobOwners.get(1);
				jobOwners.clear();
				jobOwners.add(currentOwner1);
				jobOwners.add(currentOwner2);
			}
			
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching job_owner.");
			throw new DBException("In getOwners", e);
		}
		return jobOwners;

	}

	public ArrayList<String> getJobIds(String jobOwner) throws DBException {
		ArrayList<String> jobIds = new ArrayList<String>();
		try {
			getJobIdsForOwnerPS.setString(1, jobOwner);
			ResultSet jobIdsResultSet = getJobIdsForOwnerPS.executeQuery();
			while (jobIdsResultSet.next()) {
				jobIds.add(jobIdsResultSet.getString("job_id"));
				//DELETE THIS ***********************************************************************************
				if(jobIds.size()>2)
					break;
			}

			if (jobIdsResultSet != null) {
				jobIdsResultSet.close();
				jobIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching job_id.");
			throw new DBException("In getJobIds", e);
		}
		return jobIds;

	}
	


	public ArrayList<String> getPackIds(String jobId) throws DBException {
		ArrayList<String> packIds = new ArrayList<String>();
		try {
			getPackIdsForJobIdPS.setString(1, jobId);
			ResultSet packIdsResultSet = getPackIdsForJobIdPS.executeQuery();
			while (packIdsResultSet.next()) {
				packIds.add(packIdsResultSet.getString("pack_id"));
				//DELETE THIS ***********************************************************************************
				if(packIds.size()>2)
					break;
			}

			if (packIdsResultSet != null) {
				packIdsResultSet.close();
				packIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching pack_id.");
			throw new DBException("In getPackIds", e);
		}
		return packIds;

	}


	public ArrayList<String> getFileIds(String packId) throws DBException {
		ArrayList<String> fileIds = new ArrayList<String>();
		try {
			getFileIdsForPackIdPS.setString(1, packId);
			ResultSet fileIdsResultSet = getFileIdsForPackIdPS.executeQuery();
			while (fileIdsResultSet.next()) {
				fileIds.add(fileIdsResultSet.getString("file_id"));
				//DELETE THIS ***********************************************************************************
				if(fileIds.size()>2)
					break;
			}

			if (fileIdsResultSet != null) {
				fileIdsResultSet.close();
				fileIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching file_id.");
			throw new DBException("In getFileIds", e);
		}
		return fileIds;

	}
	



	public ArrayList<String> getExtractIds(String fileId) throws DBException {
		ArrayList<String> extractIds = new ArrayList<String>();
		try {
			getExtractIdsForFileIdPS.setString(1, fileId);
			ResultSet extractIdsResultSet = getExtractIdsForFileIdPS.executeQuery();
			while (extractIdsResultSet.next()) {
				extractIds.add(extractIdsResultSet.getString("extr_id"));
				
				//DELETE THIS ***********************************************************************************
				if(extractIds.size()>2)
					break;
			}

			if (extractIdsResultSet != null) {
				extractIdsResultSet.close();
				extractIdsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching extr_id.");
			throw new DBException("In getExtractIds", e);
		}
		return extractIds;

	}
	

	public ArrayList<String> getExtracts(String extractId) throws DBException {
		ArrayList<String> extracts = new ArrayList<String>();
		try {
			getExtractJsonsForExtractIdPS.setString(1, extractId);
			ResultSet extractsResultSet = getExtractJsonsForExtractIdPS.executeQuery();
			while (extractsResultSet.next()) {
				extracts.add(extractsResultSet.getString("extr_json"));
			}

			if (extractsResultSet != null) {
				extractsResultSet.close();
				extractsResultSet = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in fetching extr_json.");
			throw new DBException("In getExtracts", e);
		}
		return extracts;

	}
}
