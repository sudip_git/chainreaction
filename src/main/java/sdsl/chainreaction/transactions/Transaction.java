/**
 * 
 */
package sdsl.chainreaction.transactions;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;

import sdsl.chainreaction.utils.StringUtil;

/**
 * @author Sudip Das
 *
 */
public class Transaction {

	public String transactionId; // this is also the hash of the transaction.
	public PublicKey sender; // senders address/public key.
	public PublicKey reciepient; // Recipients address/public key.
	public String dataToBSigned;
	public byte[] signature; // this is to prevent anybody else from spending funds in our wallet.

	public ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
	public ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();

	private static int sequence = 0; // a rough count of how many transactions have been generated.

	// Constructor:
	public Transaction(PublicKey from, PublicKey to, String dataToBSigned, ArrayList<TransactionInput> inputs) {
		this.sender = from;
		this.reciepient = to;
		this.dataToBSigned = dataToBSigned;
		this.inputs = inputs;
	}

	// This Calculates the transaction hash (which will be used as its Id)
	private String calulateHash() {
		sequence++; // increase the sequence to avoid 2 identical transactions having the same hash
//		return StringUtil.applySha256(StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient)
//				+ Float.toString(value) + sequence);
		return StringUtil.applySha256(
				StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + dataToBSigned + sequence);
	}

	// Signs all the data we dont wish to be tampered with.
	public void generateSignature(PrivateKey privateKey) {
//		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + Float.toString(value)	;
		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + dataToBSigned;
		signature = StringUtil.applyECDSASig(privateKey, data);
	}

	// Verifies the data we signed hasnt been tampered with
	public boolean verifySignature() {
//		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient)
//				+ Float.toString(value);
		String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + dataToBSigned;
		return StringUtil.verifyECDSASig(sender, data, signature);
	}

	public PublicKey getSender() {
		return sender;
	}

	public void setSender(PublicKey sender) {
		this.sender = sender;
	}

	public PublicKey getReciepient() {
		return reciepient;
	}

	public void setReciepient(PublicKey reciepient) {
		this.reciepient = reciepient;
	}

	public String getDataToBSigned() {
		return dataToBSigned;
	}

	public void setDataToBSigned(String dataToBSigned) {
		this.dataToBSigned = dataToBSigned;
	}

	public byte[] getSignature() {
		return signature;
	}

	public void setSignature(byte[] signature) {
		this.signature = signature;
	}

}
