/**
 * 
 */
package sdsl.chainreaction.blocks;

import java.util.Date;

import sdsl.chainreaction.utils.StringUtil;

/**
 * @author Sudip Das
 *
 */
public class BlockCreator {
	public String hash;
	public String previousHash;
	private String data;
	private long timeStamp;

	public BlockCreator(String data, String previousHash) {
		this.data=data;
		this.previousHash=previousHash;
		this.timeStamp=new Date().getTime();
		this.hash=calculateHash();
	}

	/**
	 * @return
	 */
	public String calculateHash() {
		String calculatedhash=StringUtil.applySha256(previousHash+Long.toString(timeStamp)+data);
		return calculatedhash;
	}
}
